package br.com.awsutils.controller;


import br.com.awsutils.Constants;
import br.com.awsutils.domain.AWSUtilsReturnMessage;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RequestMapping("/healthcheck")
@RestController
public class HealthCheckController {

    @RequestMapping(method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    public AWSUtilsReturnMessage faceCompare() {

        AWSUtilsReturnMessage returnMessage = new AWSUtilsReturnMessage();
        returnMessage.setDatetime(Constants.datetimeFormat.format(new Date()));

        long startTime = System.currentTimeMillis();

        returnMessage.setStatus(true);
        returnMessage.setMessage(Constants.SUCCESS_MESSAGE);

        returnMessage.setDuration(System.currentTimeMillis() - startTime);

        return returnMessage;

    }


}
