package br.com.awsutils.controller;


import br.com.awsutils.Constants;
import br.com.awsutils.business.RekognitionService;
import br.com.awsutils.domain.AWSUtilsReturnMessage;
import br.com.awsutils.domain.FaceMatchResult;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Date;

import static br.com.awsutils.Constants.SECURITY_MESSAGE;
import static br.com.awsutils.Constants.SECURITY_TOKEN;

@RestController
public class RekognitionController {

    @Resource
    private RekognitionService rekognitionService;

    @RequestMapping(path = "/rekognition/facematch",
            method = RequestMethod.POST,
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public AWSUtilsReturnMessage<FaceMatchResult> faceMatch(@RequestParam String token,
                                                            @RequestParam String sourceUrl,
                                                            @RequestParam String targetUrl) {

        AWSUtilsReturnMessage<FaceMatchResult> returnMessage = new AWSUtilsReturnMessage<>();
        returnMessage.setDatetime(Constants.datetimeFormat.format(new Date()));

        long startTime = System.currentTimeMillis();

        try {

            if (SECURITY_TOKEN.equals(token)) {

                double similarity = rekognitionService.faceMatch(sourceUrl, targetUrl);

                FaceMatchResult result = new FaceMatchResult();
                result.setSimilarity(similarity);

                returnMessage.setStatus(true);
                returnMessage.setMessage(Constants.SUCCESS_MESSAGE);
                returnMessage.setData(result);

            } else {

                returnMessage.setStatus(false);
                returnMessage.setMessage(SECURITY_MESSAGE);

            }

        } catch (Exception e) {

            returnMessage.setStatus(false);
            returnMessage.setMessage(e.getMessage());

        }

        returnMessage.setDuration(System.currentTimeMillis() - startTime);

        return returnMessage;

    }

    @RequestMapping(path = "/rekognition/facecompare",
                    method = RequestMethod.POST,
                    produces = {MediaType.APPLICATION_JSON_VALUE})
    public AWSUtilsReturnMessage faceCompare(@RequestParam String sourceUrl,
                                             @RequestParam String targetUrl,
                                             @RequestParam boolean detectUnsafe) {

        AWSUtilsReturnMessage returnMessage = new AWSUtilsReturnMessage();
        returnMessage.setDatetime(Constants.datetimeFormat.format(new Date()));

        long startTime = System.currentTimeMillis();

        try {

            boolean faceMatch = rekognitionService.compareFaces(sourceUrl, targetUrl, detectUnsafe);
            returnMessage.setStatus(faceMatch);
            if (faceMatch) {
                returnMessage.setMessage(Constants.FACE_MATCH);
            } else {
                returnMessage.setMessage(Constants.FACE_MISMATCH);
            }

        } catch (Exception e) {

            returnMessage.setStatus(false);
            returnMessage.setMessage(e.getMessage());

        }

        returnMessage.setDuration(System.currentTimeMillis() - startTime);

        return returnMessage;

    }

    @RequestMapping(path = "/rekognition/ocr",
                    method = RequestMethod.POST,
                    produces = {MediaType.APPLICATION_JSON_VALUE})
    public AWSUtilsReturnMessage<String> ocr(@RequestParam String token,
                                             @RequestParam String objectId) {

        AWSUtilsReturnMessage<String> returnMessage = new AWSUtilsReturnMessage<>();
        returnMessage.setDatetime(Constants.datetimeFormat.format(new Date()));

        long startTime = System.currentTimeMillis();

        try {

            if (SECURITY_TOKEN.equals(token)) {

                String data = rekognitionService.extractText(objectId);

                returnMessage.setStatus(true);
                returnMessage.setMessage("text successfully extracted");
                returnMessage.setData(data);

                System.out.println("### TEXT EXTRACTED: " + data);

            } else {

                returnMessage.setStatus(false);
                returnMessage.setMessage(SECURITY_MESSAGE);

            }

        } catch (Exception e) {

            returnMessage.setStatus(false);
            returnMessage.setMessage(e.getMessage());

        }

        returnMessage.setDuration(System.currentTimeMillis() - startTime);

        return returnMessage;

    }


}
