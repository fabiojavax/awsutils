package br.com.awsutils.controller;


import br.com.awsutils.Constants;
import br.com.awsutils.business.S3Service;
import br.com.awsutils.domain.AWSUtilsException;
import br.com.awsutils.domain.AWSUtilsReturnMessage;
import org.apache.commons.io.IOUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
public class S3Controller {

    @Resource
    private S3Service s3Service;

    @RequestMapping(path = "/s3/transferimage",
                    method = RequestMethod.POST,
                    produces = {MediaType.APPLICATION_JSON_VALUE})
    public AWSUtilsReturnMessage transferImage(@RequestParam String token,
                                               @RequestParam String filename) {

        AWSUtilsReturnMessage returnMessage = new AWSUtilsReturnMessage();
        returnMessage.setDatetime(Constants.datetimeFormat.format(new Date()));

        long startTime = System.currentTimeMillis();

        try {

            s3Service.transferImage(token, filename);

            returnMessage.setStatus(true);
            returnMessage.setMessage(Constants.SUCCESS_MESSAGE);

        } catch (Exception e) {

            returnMessage.setStatus(false);
            returnMessage.setMessage(e.getMessage());

        }

        returnMessage.setDuration(System.currentTimeMillis() - startTime);

        return returnMessage;

    }

    @RequestMapping(path = "/proposta/expurgoservice",
                    method = RequestMethod.POST,
                    consumes = "multipart/form-data",
                    produces = {MediaType.APPLICATION_JSON_VALUE})
    public AWSUtilsReturnMessage expurgoService(@RequestParam("accessKey") String accessKey,
                                                @RequestParam("secretKey") String secretKey,
                                                @RequestParam("file") MultipartFile file,
                                                @RequestParam("filename") String filename) {

        AWSUtilsReturnMessage returnMessage = new AWSUtilsReturnMessage();
        returnMessage.setDatetime(Constants.datetimeFormat.format(new Date()));

        long startTime = System.currentTimeMillis();

        try {

            s3Service.uploadExpurgo(accessKey,
                                    secretKey,
                                    file.getBytes(),
                                    filename);

            returnMessage.setStatus(true);
            returnMessage.setMessage(Constants.EXPURGO_SUCCESS);

        } catch (Exception e) {

            returnMessage.setStatus(false);
            returnMessage.setMessage(e.getMessage());

        }

        returnMessage.setDuration(System.currentTimeMillis() - startTime);

        return returnMessage;

    }

    @RequestMapping(path = "/proposta/getobject",
            method = RequestMethod.GET,
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public AWSUtilsReturnMessage getObject(@RequestParam("accessKey") String accessKey,
                                           @RequestParam("secretKey") String secretKey,
                                           @RequestParam("objectId") String objectId) {

        AWSUtilsReturnMessage<String> returnMessage = new AWSUtilsReturnMessage<>();
        returnMessage.setDatetime(Constants.datetimeFormat.format(new Date()));

        long startTime = System.currentTimeMillis();

        try {

            returnMessage.setData(s3Service.getObject(accessKey, secretKey, objectId));

            returnMessage.setStatus(true);
            returnMessage.setMessage(Constants.SUCCESS_MESSAGE);

        } catch (Exception e) {

            returnMessage.setStatus(false);
            returnMessage.setMessage(e.getMessage());

        }

        returnMessage.setDuration(System.currentTimeMillis() - startTime);

        return returnMessage;

    }



}
