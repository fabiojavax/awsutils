package br.com.awsutils.business;

import br.com.awsutils.Constants;
import br.com.awsutils.domain.AWSUtilsException;
import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.*;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.net.URL;
import java.util.Date;
import java.util.logging.Logger;

@Service
public class S3Service {

    private static Logger LOGGER = Logger.getLogger(S3Service.class.getName());

    public void transferImage(String token, String sourceName) throws AWSUtilsException {

        final AmazonS3 amazonS3 = AmazonS3Client.builder().withRegion(Regions.US_EAST_1)
                .withPathStyleAccessEnabled(true).build();

        try {

            LOGGER.info("transfering object " + sourceName + "...");

            String destinationName = sourceName;

            if (destinationName.contains("-")) {
                destinationName = destinationName.replace("-", "/");
            }

            CopyObjectRequest copyObjRequest = new CopyObjectRequest(
                    Constants.BUCKET_MOBILE, sourceName, Constants.BUCKET_DOCUMENTS, destinationName);

            amazonS3.copyObject(copyObjRequest);

            DeleteObjectRequest deleteObjectRequest = new DeleteObjectRequest(Constants.BUCKET_MOBILE, sourceName);

            amazonS3.deleteObject(deleteObjectRequest);

        } catch (AmazonServiceException ase) {

            logError(ase);

            throw new AWSUtilsException(ase.getMessage());

        } catch (AmazonClientException ace) {

            throw new AWSUtilsException(ace.getMessage());

        }

    }

    public void uploadExpurgo(String awsAccessKey,
                              String awsSecretKey,
                              byte[] file,
                              String filename) throws AWSUtilsException {

        AmazonS3 amazonS3 = new AmazonS3Client(new BasicAWSCredentials(awsAccessKey, awsSecretKey));

        try {

            LOGGER.info("uploading file " + filename + "...");

            S3Object s3Object = new S3Object();

            String contentType = "image/jpeg";
            if (filename.split("\\.")[1].equalsIgnoreCase("txt")) {
                contentType = "text/plain";
            }

            ObjectMetadata omd = new ObjectMetadata();
            omd.setContentType(contentType);
            omd.setContentLength(file.length);
            omd.setHeader("filename", filename);

            ByteArrayInputStream bis = new ByteArrayInputStream(file);

            s3Object.setObjectContent(bis);
            amazonS3.putObject(new PutObjectRequest(Constants.BUCKET_DOCUMENTS, filename, bis, omd));
            s3Object.close();

        } catch (AmazonServiceException ase) {

            logError(ase);

            throw new AWSUtilsException(ase.getMessage());

        } catch (Exception e) {

            throw new AWSUtilsException(e.getMessage());

        }

    }

    public String getObject(String awsAccessKey,
                            String awsSecretKey,
                            String objectId) throws AWSUtilsException {

        try {

            BasicAWSCredentials awsCreds = new BasicAWSCredentials(awsAccessKey, awsSecretKey);
            AmazonS3 s3client = new AmazonS3Client(awsCreds);

            Date expiration = new Date();
            long msec = expiration.getTime();
            msec += 1000 * 60 * 2;
            expiration.setTime(msec);

            GeneratePresignedUrlRequest generatePresignedUrlRequest =
                    new GeneratePresignedUrlRequest(Constants.BUCKET_DOCUMENTS, objectId);
            generatePresignedUrlRequest.setExpiration(expiration);

            URL s = s3client.generatePresignedUrl(generatePresignedUrlRequest);

            return s.toString();

        } catch (AmazonServiceException ase) {

            logError(ase);

            throw new AWSUtilsException(ase.getMessage());

        } catch (Exception e) {

            throw new AWSUtilsException(e.getMessage());

        }

    }

    private void logError(AmazonServiceException e) {

        System.out.println("Error Message:    " + e.getMessage());
        System.out.println("HTTP Status Code: " + e.getStatusCode());
        System.out.println("AWS Error Code:   " + e.getErrorCode());
        System.out.println("Error Type:       " + e.getErrorType());
        System.out.println("Request ID:       " + e.getRequestId());

    }


}